var heightLoad = 0;
var stopAnimation = true;
var page = window.location.pathname.split("/");
var url = page[page.length - 1];
var pageLoad = setInterval(function () {
    if (stopAnimation && heightLoad < 81 && document.querySelector(".load")) {
        heightLoad++;
        document.querySelector(".load .load-percentage").innerHTML = heightLoad + '%';
        document.querySelector(".load .fill").style.height = 100 - heightLoad + '%';
    }
}, 200);

window.onload = function () {
    init();
};

window.onscroll = function (event) {
    scroll(event);
};

function init() {

    setTimeout(function () {
        load();
    }, 500);

    menu();
    search();
}

function load() {
    if (document.querySelector(".load")) {
        stopAnimation = false;
        clearInterval(pageLoad);

        document.querySelector(".load .load-percentage").innerHTML = '100%';
        document.querySelector(".load .fill").style.height = '0%';

        if (url === "" || url === "index" || url === "index.html") {
            setTimeout(function () {
                document.querySelector(".load").classList.add("load-disable");
            }, 500);
        }

        setTimeout(function () {
            document.querySelector(".load").classList.remove("load-disable");
            document.querySelector(".load").style.display = 'none';
        }, 1000);
    }
}


function menu() {
    var menuDesktop = document.querySelector(".menu");
    var menuBackground = document.querySelector(".menu-background");

    document.querySelector(".menu-mobile").addEventListener('click', function () {
        setTimeout(function () {
            menuDesktop.classList.add('menu-active');
        }, 200);
        menuBackground.classList.add('menu-background-active');
    });

    document.querySelector(".close").addEventListener('click', function () {
        menuBackground.classList.remove('menu-background-active');
        menuDesktop.classList.remove('menu-active');
    });

    menuBackground.addEventListener('click', function () {
        menuBackground.classList.remove('menu-background-active');
        menuDesktop.classList.remove('menu-active');
    });
}

function scroll(event) {
    var validation = (document.documentElement.scrollTop > 100 || document.body.scrollTop > 100) && (window.innerWidth > 1100);
    var navagation = document.querySelector("nav");

    if (validation) {
        navagation.classList.add('nav-active');
    } else {
        navagation.classList.remove('nav-active');
    }
}


function search() {
    document.getElementsByClassName('btn-search')[0].children[0].addEventListener('click', function () {
        document.getElementsByClassName('search')[0].classList.add('search-active');
        document.getElementsByClassName('input-search')[0].focus();
    });

    document.getElementsByClassName('btn-close-search')[0].addEventListener('click', function () {
        document.getElementsByClassName('search')[0].classList.remove('search-active');
    });

    document.onkeypress = function (event) {
        if (event['keyCode'] === 27) {
            document.getElementsByClassName('search')[0].classList.remove('search-active');
        }
    }

}
